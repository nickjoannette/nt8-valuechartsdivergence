#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.NinjaScript.DrawingTools;
#endregion

//This namespace holds Indicators in this folder and is required. Do not change it. 
namespace NinjaTrader.NinjaScript.Indicators.Sim22
{
	/// <summary>
	/// Sim22 Jan 2016 NT8b8. Based on Tradestation Value Chart by David Stendahl.
	/// </summary>
	public class Sim22_ValueChartV1 : Indicator
	{
		SMA							lrSMA;
		SMA							vcSMA;
		private Series<double>		var0;
		private Series<double>		op;
		private Series<double>		hi;
		private Series<double>		lo;
		
		protected override void OnStateChange()
		{
			if (State == State.SetDefaults)
			{
				Description							= @"Based on Tradestation Value Chart by David Stendahl. Sim22 Jan 2016 NT8b9.";
				Name								= "ValueChartSim22";
				Calculate							= Calculate.OnPriceChange;
				IsOverlay							= false;
				DisplayInDataBox					= true;
				DrawOnPricePanel					= false;
				DrawHorizontalGridLines				= true;
				DrawVerticalGridLines				= true;
				PaintPriceMarkers					= true;
				ScaleJustification					= NinjaTrader.Gui.Chart.ScaleJustification.Right;
				//Disable this property if your indicator requires custom values that cumulate with each new market data event. 
				//See Help Guide for additional information.
				IsSuspendedWhileInactive			= true;
				
				AddPlot(new Stroke(Brushes.Black, 1f), PlotStyle.Hash, "VClose");
				AddLine(new Stroke(Brushes.Red, DashStyleHelper.Dot, 1), 8, "8");
				AddLine(new Stroke(Brushes.DarkCyan, DashStyleHelper.Dot, 1), 4, "4");
				AddLine(new Stroke(Brushes.DarkCyan, DashStyleHelper.Dot, 1), -4, "-4");
				AddLine(new Stroke(Brushes.Green, DashStyleHelper.Dot, 1), -8, "-8");
				
				UpBarBrush							= Brushes.LimeGreen;
				DownBarBrush						= Brushes.Red;
				OutlineBarBrush						= Brushes.Black;
				
				numberBars							= 5;
				opacity								= 15;
			}
			else if (State == State.Configure)
			{
				var0	= new Series<double>(this, MaximumBarsLookBack.Infinite);
				op		= new Series<double>(this, MaximumBarsLookBack.Infinite);
				hi		= new Series<double>(this, MaximumBarsLookBack.Infinite);
				lo		= new Series<double>(this, MaximumBarsLookBack.Infinite);
				lrSMA	= SMA(var0, 5);
				vcSMA	= SMA(Median, numberBars);
				
				UpBarBrush.Freeze();
				DownBarBrush.Freeze();
				OutlineBarBrush.Freeze();
			}
		}
		
		public override string DisplayName
		{
		    get { return "ValueChart (" + numberBars + ")"; }
		}
		
		public override void OnCalculateMinMax()
		{  
			if (Bars == null) 
				return;
			
			int lastBar = ChartBars.ToIndex;
			int firstBar = ChartBars.FromIndex;

			double min = -10d;
			double max = 10d;

			for (int index = firstBar; index <= lastBar; index++)
			{
				{
					min = Math.Min(min, lo.GetValueAt(index));
					max = Math.Max(max, hi.GetValueAt(index));
				}
			}

			if ((max - min) < 1)
			{
				min -= 1;
				max += 1;
			}
			
		   MinValue = min;
		   MaxValue = max;
		} 
		

#region		Calculate VChart
		
		private double VChart(int numBars)
		{
			double	varA, varB, varC, varD, varE;
			double	varR1, varR2, varR3, varR4, varR5;
			
			int	varP		= (int)Math.Round((double)numBars/5, 0);
			double	LRange	= 0;
			double	vChart	= 0;
			
			if (numBars > 7)
			{
				varA	= MAX(High, varP)[0] - MIN(Low, varP)[0];
				if (varA == 0 && varP == 1)
					varR1	= Math.Abs(Close[0] - Close[varP]);
				else
					varR1	= varA;
				
				varB	= MAX(High, varP)[varP+1] - MIN(Low, varP)[varP];
				if (varB == 0 && varP == 1)
					varR2	= Math.Abs(Close[varP] - Close[varP*2]);
				else
					varR2	= varB;
				
				varC	= MAX(High, varP)[varP*2] - MIN(Low, varP)[varP*2];
				if (varC == 0 && varP == 1)
					varR3	= Math.Abs(Close[varP*2] - Close[varP*3]);
				else
					varR3	= varC;
				
				varD	= MAX(High, varP)[varP*3] - MIN(Low, varP)[varP*3];
				if (varD == 0 && varP == 1)
					varR4	= Math.Abs(Close[varP*3] - Close[varP*4]);
				else
					varR4	= varD;
				
				varE	= MAX(High, varP)[varP*4] - MIN(Low, varP)[varP*4];
				if (varE == 0 && varP == 1)
					varR5	= Math.Abs(Close[varP*4] - Close[varP*5]);
				else
					varR5	= varE;
				LRange	= ((varR1+varR2+varR3+varR4+varR5)/5)*.2;
			}
			else
			{
				if (Math.Abs(Close[0] - Close[1]) > (High[0] - Low[0]))
					var0[0]	= Math.Abs(Close[0] - Close[1]);
				else
					var0[0]	= High[0] - Low[0];
				if (High[0] == Low[0])
					var0[0]	= Math.Abs(Close[0] - Close[1]);
				LRange	= lrSMA[0] * 0.2;
		
			}
			if (LRange == 0)
				return 0.0;
			
			return LRange;

		}
#endregion
		protected override void OnBarUpdate()
		{
			if (CurrentBar <= numberBars)
				return;
			double 	close0	= Close[0];
			double 	open0	= Open[0];
			double	high0	= High[0];
			double	low0	= Low[0];
			
			//Improved by calling VChart once per bar update for all OHLC values.
			double vch		= VChart(numberBars);
			
			Value[0]	= ((close0 - vcSMA[0]))/(vch);
			op[0]		= ((open0 - vcSMA[0]))/(vch);
			hi[0]		= ((high0 - vcSMA[0]))/(vch);
			lo[0]		= ((low0 - vcSMA[0]))/(vch);
			
			// For price marker
			Plots[0].Brush	= Value[0] >= op[0] ? UpBarBrush : DownBarBrush;
		}
		
		protected override void OnRender(ChartControl chartControl, ChartScale chartScale)
		{
			try
			{	
				if( Bars == null || ChartControl == null || Bars.Instrument == null || !IsVisible) 
				{
					return;
				}
	            //base.OnRender(chartControl, chartScale);
				
				NinjaTrader.Data.Bars	bars	= ChartBars.Bars;
				
				int cbti = Calculate == Calculate.OnBarClose ? ChartBars.ToIndex - 1 : ChartBars.ToIndex;
				int cbfi = ChartBars.FromIndex > numberBars ? ChartBars.FromIndex : numberBars;
				
				SharpDX.Direct2D1.Brush		upBrush		= UpBarBrush.ToDxBrush(RenderTarget);
				SharpDX.Direct2D1.Brush		dnBrush		= DownBarBrush.ToDxBrush(RenderTarget);
				SharpDX.Direct2D1.Brush		olBrush		= OutlineBarBrush.ToDxBrush(RenderTarget);
				SharpDX.Direct2D1.Brush		hiBrush		= Lines[0].BrushDX;
				SharpDX.Direct2D1.Brush		muBrush		= Lines[1].BrushDX;
				SharpDX.Direct2D1.Brush		mlBrush		= Lines[2].BrushDX;
				SharpDX.Direct2D1.Brush		loBrush		= Lines[3].BrushDX;
				
				// Regions
				
				SharpDX.Direct2D1.StrokeStyle	hiSS	= Lines[0].StrokeStyle;
				SharpDX.Direct2D1.StrokeStyle	muSS	= Lines[1].StrokeStyle;
				SharpDX.Direct2D1.StrokeStyle	mlSS	= Lines[2].StrokeStyle;
				SharpDX.Direct2D1.StrokeStyle	loSS	= Lines[3].StrokeStyle;
				
				float	upperY							= chartScale.GetYByValue(Lines[0].Value);
				float	midupperY						= chartScale.GetYByValue(Lines[1].Value);
				float	midlowerY						= chartScale.GetYByValue(Lines[2].Value);
				float	lowerY							= chartScale.GetYByValue(Lines[3].Value);
				
				// Upper Line
				RenderTarget.DrawLine(new SharpDX.Vector2(ChartPanel.X, upperY), new SharpDX.Vector2(ChartPanel.W, upperY), hiBrush, Lines[0].Width, hiSS);
				// Mid Upper Line
				RenderTarget.DrawLine(new SharpDX.Vector2(ChartPanel.X, midupperY), new SharpDX.Vector2(ChartPanel.W, midupperY), muBrush, Lines[1].Width, muSS);
				// Mid Lower Line
				RenderTarget.DrawLine(new SharpDX.Vector2(ChartPanel.X, midlowerY), new SharpDX.Vector2(ChartPanel.W, midlowerY), mlBrush, Lines[2].Width, mlSS);
				// Lower Line
				RenderTarget.DrawLine(new SharpDX.Vector2(ChartPanel.X, lowerY), new SharpDX.Vector2(ChartPanel.W, lowerY), loBrush, Lines[3].Width, loSS);
				
				SharpDX.RectangleF		upperRect		= new SharpDX.RectangleF();
				upperRect.X								= ChartPanel.X;
				upperRect.Y								= upperY;
				upperRect.Width							= ChartPanel.W;
				upperRect.Height						= midupperY - upperY;
				hiBrush.Opacity							= opacity * 0.01f;
				RenderTarget.FillRectangle(upperRect, hiBrush);
				hiBrush.Opacity							= 100f;
				// MidRegion
				SharpDX.RectangleF		midRect			= new SharpDX.RectangleF();
				midRect.X								= ChartPanel.X;
				midRect.Y								= midupperY;
				midRect.Width							= ChartPanel.W;
				midRect.Height							= midlowerY - midupperY;
				muBrush.Opacity						= opacity * 0.01f;
				RenderTarget.FillRectangle(midRect, muBrush);
				//RenderTarget.DrawRectangle(rect, olBrush, 1f);

				// LoweRegion
				SharpDX.RectangleF		lowerRect		= new SharpDX.RectangleF();
				lowerRect.X								= ChartPanel.X;
				lowerRect.Y								= midlowerY;
				lowerRect.Width							= ChartPanel.W;
				lowerRect.Height						= lowerY - midlowerY;
				loBrush.Opacity							= opacity * 0.01f;
				RenderTarget.FillRectangle(lowerRect, loBrush);
				
				SharpDX.Vector2			point0	= new SharpDX.Vector2();
				SharpDX.Vector2			point1	= new SharpDX.Vector2();
				///HLC		
				SharpDX.Vector2			point2	= new SharpDX.Vector2();
				SharpDX.Vector2			point3	= new SharpDX.Vector2();
				///Candlesticks
				SharpDX.RectangleF		rect	= new SharpDX.RectangleF();
				
				for (int idx = cbfi; idx <= cbti ; idx++)
				{
					
					double					OpValue			= op.GetValueAt(idx);
					double					HiValue			= hi.GetValueAt(idx);
					double					LoValue			= lo.GetValueAt(idx);
					double					ClValue			= Values[0].GetValueAt(idx);
		
					//in Pixels
					float					open			= chartScale.GetYByValue(OpValue);
					float					high			= chartScale.GetYByValue(HiValue);
					float					low				= chartScale.GetYByValue(LoValue);
					float					close			= chartScale.GetYByValue(ClValue);
					int						x				= chartControl.GetXByBarIndex(ChartBars, idx);
					
					
					if (ChartBars.Properties.ChartStyleType	!= ChartStyleType.OHLC) //Candles
		            {
						RenderTarget.AntialiasMode	= SharpDX.Direct2D1.AntialiasMode.Aliased; //
						
		                float barWidth 	= (float) chartControl.GetBarPaintWidth(chartControl.BarsArray[0]);
						
						if (Math.Abs(open - close) < 0.0000001) //Doji
						{
							// Line 
							point0.X = x - barWidth * 0.5f;
							point0.Y = close;
							point1.X = x + barWidth * 0.5f;
							point1.Y = close;
							RenderTarget.DrawLine(point0, point1, olBrush, 1f);
						}
						else
						{
							// Candle
							SharpDX.Direct2D1.Brush 	brushC	= ClValue >= OpValue ? upBrush : dnBrush;
							rect.X		= x - barWidth * 0.5f + 0.5f;
							rect.Y		= Math.Min(close, open);
							rect.Width	= barWidth - 1;
							rect.Height	= Math.Max(open, close) - Math.Min(close, open);
							RenderTarget.FillRectangle(rect, brushC);
							RenderTarget.DrawRectangle(rect, olBrush, 1f);
						}
						// High wick
						if (high < Math.Min(open, close))
						{
							point0.X = x;
							point0.Y = high;
							point1.X = x;
							point1.Y = Math.Min(open, close);
							RenderTarget.DrawLine(point0, point1, olBrush, 1f);
						}
						// Low wick
						if (low > Math.Max(open, close))
						{
							point0.X = x;
							point0.Y = low;
							point1.X = x;
							point1.Y = Math.Max(open, close);
							RenderTarget.DrawLine(point0, point1, olBrush, 1f);
						}
					}
					else
					{	
						RenderTarget.AntialiasMode	= SharpDX.Direct2D1.AntialiasMode.Aliased; //
						
						float	lineWidth 	= (float) Math.Max(chartControl.BarWidth, 1.5);
						point0.X = x - lineWidth * 0.5f;
						point0.Y = high - lineWidth * 0.5f;
						point1.X = x - lineWidth * 0.5f;
						point1.Y = low + lineWidth * 0.5f;

						RenderTarget.DrawLine(point0, point1, ClValue >= OpValue ? upBrush : dnBrush, lineWidth);
						//RenderTarget.DrawLine(point0, point1, olBrush, lineWidth);

						point2.X = x - lineWidth * 0.5f + lineWidth + 2;
						point2.Y = close;
						point3.X = x - lineWidth * 0.5f;
						point3.Y = close;

						//RenderTarget.DrawLine(point2, point3, olBrush, lineWidth);
						RenderTarget.DrawLine(point2, point3, ClValue >= OpValue ? upBrush : dnBrush, lineWidth);
					}
				}
				upBrush.Dispose();
				dnBrush.Dispose();
				olBrush.Dispose();
				muBrush.Dispose();
				mlBrush.Dispose();
				hiBrush.Dispose();
				loBrush.Dispose();
				hiSS.Dispose();
				loSS.Dispose();
				muSS.Dispose();
				mlSS.Dispose();
			}
			catch{}
		}
		
		[Browsable(false)]
		[XmlIgnore()]
		public Series<double> VClose
		{
			get { return Values[0]; }
		}
		
		[Browsable(false)]
		[XmlIgnore()]
		public Series<double> VOpen
		{
			get { return op; }
		}
		
		[Browsable(false)]
		[XmlIgnore()]
		public Series<double> VHigh
		{
			get { return hi; }
		}
		
		[Browsable(false)]
		[XmlIgnore()]
		public Series<double> VLow
		{
			get { return lo; }
		}
		
		[Range(2, 1000), NinjaScriptProperty]
		[Display(ResourceType = typeof(Custom.Resource), Name = "Number of bars", GroupName = "NinjaScriptParameters", Order = 0)]
		public int numberBars 
		{ get; set; }
		
		[Range(1, 100)]
		[NinjaScriptProperty]
		[Display(Name="Band opacity", Description="Opacity 1 - 100", Order=3, GroupName="1. Colors")]
		public int opacity
		{ get; set; }	
		
		[XmlIgnore]
		[Display(Name="Up bar color", Description="Color for up bar", Order=0, GroupName="1. Colors")]
		public Brush UpBarBrush
		{ get; set; }

		[Browsable(false)]
		public string UpBarBrushSerializable
		{
			get { return Serialize.BrushToString(UpBarBrush); }
			set { UpBarBrush = Serialize.StringToBrush(value); }
		}	
		
		[XmlIgnore]
		[Display(Name="Down bar color", Description="Color for down bar", Order=1, GroupName="1. Colors")]
		public Brush DownBarBrush
		{ get; set; }

		[Browsable(false)]
		public string DownBarBrushSerializable
		{
			get { return Serialize.BrushToString(DownBarBrush); }
			set { DownBarBrush = Serialize.StringToBrush(value); }
		}
		
		[XmlIgnore]
		[Display(Name="Outline color", Description="Color for wick and outline", Order=2, GroupName="1. Colors")]
		public Brush OutlineBarBrush
		{ get; set; }

		[Browsable(false)]
		public string OutlineBarBrushSerializable
		{
			get { return Serialize.BrushToString(OutlineBarBrush); }
			set { OutlineBarBrush = Serialize.StringToBrush(value); }
		}	
	}
	
}

#region NinjaScript generated code. Neither change nor remove.

namespace NinjaTrader.NinjaScript.Indicators
{
	public partial class Indicator : NinjaTrader.Gui.NinjaScript.IndicatorRenderBase
	{
		private Sim22.Sim22_ValueChartV1[] cacheSim22_ValueChartV1;
		public Sim22.Sim22_ValueChartV1 Sim22_ValueChartV1(int numberBars, int opacity)
		{
			return Sim22_ValueChartV1(Input, numberBars, opacity);
		}

		public Sim22.Sim22_ValueChartV1 Sim22_ValueChartV1(ISeries<double> input, int numberBars, int opacity)
		{
			if (cacheSim22_ValueChartV1 != null)
				for (int idx = 0; idx < cacheSim22_ValueChartV1.Length; idx++)
					if (cacheSim22_ValueChartV1[idx] != null && cacheSim22_ValueChartV1[idx].numberBars == numberBars && cacheSim22_ValueChartV1[idx].opacity == opacity && cacheSim22_ValueChartV1[idx].EqualsInput(input))
						return cacheSim22_ValueChartV1[idx];
			return CacheIndicator<Sim22.Sim22_ValueChartV1>(new Sim22.Sim22_ValueChartV1(){ numberBars = numberBars, opacity = opacity }, input, ref cacheSim22_ValueChartV1);
		}
	}
}

namespace NinjaTrader.NinjaScript.MarketAnalyzerColumns
{
	public partial class MarketAnalyzerColumn : MarketAnalyzerColumnBase
	{
		public Indicators.Sim22.Sim22_ValueChartV1 Sim22_ValueChartV1(int numberBars, int opacity)
		{
			return indicator.Sim22_ValueChartV1(Input, numberBars, opacity);
		}

		public Indicators.Sim22.Sim22_ValueChartV1 Sim22_ValueChartV1(ISeries<double> input , int numberBars, int opacity)
		{
			return indicator.Sim22_ValueChartV1(input, numberBars, opacity);
		}
	}
}

namespace NinjaTrader.NinjaScript.Strategies
{
	public partial class Strategy : NinjaTrader.Gui.NinjaScript.StrategyRenderBase
	{
		public Indicators.Sim22.Sim22_ValueChartV1 Sim22_ValueChartV1(int numberBars, int opacity)
		{
			return indicator.Sim22_ValueChartV1(Input, numberBars, opacity);
		}

		public Indicators.Sim22.Sim22_ValueChartV1 Sim22_ValueChartV1(ISeries<double> input , int numberBars, int opacity)
		{
			return indicator.Sim22_ValueChartV1(input, numberBars, opacity);
		}
	}
}

#endregion
